# libNDI

**NDI is fun, let's use it!**

**libNDI** is a new **NDI** cross-platform, open-source library done to interact with NDI streams.

The canonical repository URL for this repo is https://code.videolan.org/jbk/libndi .

# Goal and Features

The goal of this project is to provide a way to interact with NDI streams, without requiring to use a non-open-source SDK, and targetting numerous platforms.

The library is now at an alpha level of quality, do not use in production :)

## License

**libNDI** is released under the LGPL, like FFmpeg or libVLC.

# Legality

This **libNDI** library was produced using some reverse-engineering that is allowed by the French law, in the case of interoperability.

En France, la directive européenne *91/250/CEE* a été transposée par la loi *n° 94-361 du 10 mai 1994* et le terme interopérabilité, mentionné à l'article *L. 122-6-1*
du **Code de la propriété intellectuelle**.

Il est notamment écrit à l ’article *L. 122-6-1, III*, du **Code de la propriété intellectuelle**, rédigé de la
manière suivante : « la personne ayant le droit d’utiliser le logiciel peut sans l’autorisation de l’auteur 
observer, étudier ou tester le fonctionnement de ce logiciel afin de déterminer les idées et principes qui sont
à la base de n’importe quel élément du logiciel lorsqu’elle effectue toute opération de chargement,
d’affichage, d’exécution, de transmission ou de stockage du logiciel qu’elle est en droit d’effectuer&nbsp;».

Il est aussi écrit, à l ’article *L. 122-6-1, IV*, du **Code de la propriété intellectuelle** :

« IV. La reproduction du code du logiciel ou la traduction de la forme de ce code n’est pas soumise à
l’autorisation de l’auteur lorsque la reproduction ou la traduction au sens du
1° ou du 2° de l’article L. 122-6 est indispensable pour obtenir les
informations nécessaires à l’interopérabilité d’un logiciel créé de façon indépendante avec d’autres logiciels&nbsp;».

There is no limitation of distributing this information to third parties, in other countries.

# Contribute

## CoC

The [VideoLAN Code of Conduct](https://wiki.videolan.org/CoC) applies to this project.

## TODO
- Remove libavutil dependency (av_fifo_\*)
- Fuzzing
- Support NDI-HX and udp variant
- Sending
